//EX1: Tính thuế thu nhập cá nhân

/*
Input: Tổng thu nhập năm : giả sử 10tr/tháng --> 1 năm = 10 *12 = 120.000.000 

110tr thì sẽ chịu thuế 10% --> Tiền thuế = 110tr * 0.1



Dùng type number khi nhập 120.000.000 --> có thể ghi ngắn gọn 120e + 6; 4tr --> 4e + 6;--> 4e +6 ????????? e ????
 
thuNhapChiuThue = tongThuNhapNam -4tr - soNguoiPhuThuoc * 1.6

tienThueThuNhapCaNhan = thuNhapChiuThue * thueSuat;
*/
function ketQua() {
  var hoTen = document.getElementById("hoTen").value;
  var tongThuNhapNam = document.getElementById("tongThuNhapNam").value * 1;
  var soNguoiPhuThuoc = document.getElementById("soNguoiPhuThuoc").value * 1;

  // Áp dụng công thức tính thu nhập chịu thuế
  var thuNhapChiuThue = tongThuNhapNam - 4000000 - soNguoiPhuThuoc * 1600000;
  // gọi thueSuat từ function
  var thueSuat = kiemTraThueSuat(thuNhapChiuThue);
  // Áp dụng công thức
  var tienThueThuNhapCaNhan = thuNhapChiuThue * thueSuat;
  // In ra màn hình
  if (thuNhapChiuThue < 0) {
    alert("Số tiền thu nhập không hợp lệ");
  } else {
    document.getElementById("ketQua").innerHTML =
      "Họ tên: " +
      hoTen +
      " Tiền thuế thu nhập cá nhân: " +
      new Intl.NumberFormat().format(tienThueThuNhapCaNhan) +
      "VNĐ";
  }
}

function kiemTraThueSuat(x) {
  if (x <= 60000000) {
    y = 0.05;
  } else if (x <= 120000000) {
    y = 0.1;
  } else if (x <= 210000000) {
    y = 0.15;
  } else if (x <= 384000000) {
    y = 0.2;
  } else if (x <= 624000000) {
    y = 0.25;
  } else if (x <= 960000000) {
    y = 0.3;
  } else {
    y = 0.35;
  }
  return y;
}

//////////////////////////////////////////////////////////

//EX2: Tính tiền cáp

function tinhTienCap() {
  var chonKhachHang = document.getElementById("selection").value;
  var maKhachHang = document.getElementById("maKhachHang").value;
  var soKenhCaoCap = document.getElementById("soKenhCaoCap").value * 1;

  if (chonKhachHang == 1) {
    var tienCap = nhaDan(soKenhCaoCap);
  }
  if (chonKhachHang == 2) {
    var soKetNoi = document.getElementById("soKetNoi").value * 1;
    var tienCap = doanhNghiep(soKenhCaoCap, soKetNoi);
  }
  document.getElementById(
    "ketQua1"
  ).innerHTML = `Mã khách hàng ${maKhachHang}; Tiền cáp: ${tienCap}$`;
}

function nhaDan(soKenh) {
  var phiXulyHoaDon = 4.5;
  var phiDichVuCoBan = 20.5;
  var phiThueKenhCaoCapMoiKenh = 7.5;

  var tienCap =
    phiDichVuCoBan + phiXulyHoaDon + phiThueKenhCaoCapMoiKenh * soKenh;

  return tienCap;
}

function doanhNghiep(soKenh, soLan) {
  var phiXulyHoaDon = 15;
  var phiDichVuCoBan10KetNoiDau = 75;
  var phiThueKenhCaoCapMoiKenh = 50;
  var phiKetNoiThemMoiLan = 5;

  if (soLan > 10) {
    var tienCap =
      phiXulyHoaDon +
      phiDichVuCoBan10KetNoiDau +
      phiThueKenhCaoCapMoiKenh * soKenh +
      phiKetNoiThemMoiLan * (soLan - 10);
  } else {
    var tienCap =
      phiXulyHoaDon +
      phiDichVuCoBan10KetNoiDau +
      phiThueKenhCaoCapMoiKenh * soKenh;
  }
  return tienCap;
}

document.getElementById("soKetNoi").style.display = "none";
function kichHoat() {
  var x = document.getElementById("selection").value;
  document.getElementById("soKetNoi").style.display = "none";
  if (x == 2) {
    document.getElementById("soKetNoi").style.display = "block";
  }
}

/*
                  
                  Input: Khách hàng 2 loại: nhà dân và doanh nghiệp
                  1. Nhà dân:
                  Phí xử lý hóa đơn 4.5
                  Phí dịch vụ cơ bản 20.5
                  Thuê kênh cao cấp 7.5/kênh
                  2. Doanh nghiệp:
                  Phí xử lí hóa đơn 15
                  Phí dịch vụ cơ bản 75--cho tổng 10 kết nối đầu
                  mỗi kết nối thêm 5/kết nối
                  Thuê cao cấp 50/kênh
                  Nếu nhập vào doanh nghiệp thì ô kết nối sẽ hiện lên còn ô nhà dân thì ô kết nối sẽ biến mất
                  
                  
                  
                  Sử dụng event onchange của dropdown để xử lí ẩn hiện cho ô kết nối
                  Khách hàng doanh nghiệp: 10 kết nối đầu hoặc nhỏ hơn đều đóng 75
                  kết 11 trở đi sẽ tính thêm 5/kết nối
                  */
